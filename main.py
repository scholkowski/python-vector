import vector as v

def main():
    v1 = v.Vector(value = [1,2,3])
    v2 = v.Vector(value = [0.5,0.5,0.5])
    v3 = v.Vector(value = [1,0,0])

    #add
    assert v1.add(v2).value == [1.5,2.5,3.5]

    #sub
    assert v1.sub(v2).value == [0.5,1.5,2.5]

    #mul
    assert v1.mul(2).value == [2,4,6]

    #dot
    assert v1.dot(v2) == 3

    #dist
    assert v1.dist(v1) == 0

    #len
    assert v3.mag() == 1

    #norm
    assert v1.norm().mag() == 1

    #ndim
    assert v1.ndim() == 3

if __name__ == "__main__":
    main()